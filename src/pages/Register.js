import { useState, useEffect, useContext } from 'react';
import { Form, Button } from 'react-bootstrap';
import { Navigate } from 'react-router-dom';
import UserContext from '../UserContext';

import Swal from 'sweetalert2';

export default function Register(){

	const { user } = useContext(UserContext);

	// State hooks to store the values of the input fields
	// getters are variables that store data (from setters)
	// setters are function that sets the data (for the getters)

	const [ successRegistration, setSuccessRegistration ] = useState();
	const [firstName, setFirstName] = useState('');
	const [lastName, setLastName] = useState('');
	const [email, setEmail] = useState('');
	const [mobileNo, setMobileNo] = useState('');
	const [password1, setPassword1] = useState('');
	const [password2, setPassword2] = useState('');
	// State to determine whether submit button is enabled or not
	const [isActive, setIsActive] = useState(false);

	// Function to simulate user registration
	function registerUser(e) {
		// Prevents page redirection via form submission
		e.preventDefault();

		fetch(`${process.env.COURSE_BOOKING_APP_API_URL}/users/checkEmail`, {
			method: "POST",
			headers: {
				"Content-Type": "application/json",
			},
			body: JSON.stringify({
				email: email,
			}),
		})
			.then((res) => res.json())
			.then((data) => {

			if(!data){
		
				fetch(`${process.env.COURSE_BOOKING_APP_API_URL}/users/register`,
				{
					method: 'POST',
					headers: {
						'Content-Type': 'application/json'
					},
					body: JSON.stringify({
						firstName: firstName,
						lastName: lastName,
						email: email,
						mobileNo: mobileNo,
						password: password1,

					})
				})
				.then(res => res.json())
				.then(data => {

					// It is good practice to always print out the result of our fetch request to ensure that the correct information is received in our frontend application

					// If no user information is found, the "access" property will not be available and will return undefined
					// Using the typeof operator will return a string of the data type of the variable/expression it preceeds which is why the value being compared is in a string data type
					if(data){
						// Implement sweetalert2 for login page and add condition
						setSuccessRegistration(data);
						setFirstName('');
						setLastName('');
						setMobileNo('');
						setEmail('');
						setPassword1('');
						setPassword2('');
						Swal.fire({
							title: "Login Successful",
							icon: "success",
							text: "Welcome to Zuitt!"
						});
					} 
				});
			} else {

				Swal.fire({
					title: "Authentication Failed",
					icon: "error",
					text: "Duplicate Email!"
				});
			}
		})
	}

	useEffect(() => {
		// Validation to enable submit button when all fields are populated and both passwords match
		if((firstName !== '' && lastName !== '' && email !== '' && mobileNo !== '' && password1 !== '' && password2 !== '') && (password1 === password2)){
			setIsActive(true);
		} else {
			setIsActive(false);
		}
	}, [firstName, lastName, email, mobileNo, password1, password2]);

	return successRegistration ? ( 
	<Navigate to="/login" /> )
	: 
	(
		// 2-way Binding (Bnding the user input states into their corresponding input fields via the onChange JSX Event Handler)
		<Form onSubmit={(e) => registerUser(e)}>
			<Form.Group className="mb-3" controlId="userFirstName">
				<Form.Label>First Name</Form.Label>
				<Form.Control 
					type="text"
					placeholder="Enter first name"
					value={ firstName }
					onChange={e => setFirstName(e.target.value)}
					required/>
			</Form.Group>

			<Form.Group className="mb-3" controlId="userLastName">
				<Form.Label>Last Name</Form.Label>
				<Form.Control 
					type="text" 
					placeholder="Enter last name"
					value={ lastName }
					onChange={e => setLastName(e.target.value)}
					required/>
			</Form.Group>

			<Form.Group className="mb-3" controlId="userMobileNo">
				<Form.Label>Mobile No</Form.Label>
				<Form.Control 
					type="tel" 
					placeholder="Enter mobile no"
					value={ mobileNo }
					onChange={e => setMobileNo(e.target.value)}
					required/>
				<Form.Text className="text-muted">
					Please enter your mobile no with at least 11 digits.
				</Form.Text>
			</Form.Group>

			<Form.Group className="mb-3" controlId="userEmail">
				<Form.Label>Email address</Form.Label>
				<Form.Control 
					type="email" 
					placeholder="Enter email"
					value={ email }
					onChange={e => setEmail(e.target.value)}
					required/>
				<Form.Text className="text-muted">
				We'll never share your email with anyone else.
				</Form.Text>
			</Form.Group>

			<Form.Group className="mb-3" controlId="password1">
				<Form.Label>Password</Form.Label>
				<Form.Control 
					type="password" 
					placeholder="Password"
					value={ password1 }
					onChange={e => setPassword1(e.target.value)} 
					required/>
			</Form.Group>
			
			<Form.Group className="mb-3" controlId="password2">
				<Form.Label>Verify Password</Form.Label>
				<Form.Control 
					type="password" 
					placeholder="Verify Password"
					value={ password2 }
					onChange={e => setPassword2(e.target.value)}  
					required/>
			</Form.Group>

			{ isActive ?
				<Button variant="primary" type="submit" id="submitBtn">
				Submit
				</Button>
				:
				<Button variant="danger" type="submit" id="submitBtn" disabled>
				Submit
				</Button>
			}
		</Form>
	)
}
